package net.zoneland.zoneapp;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class Tab1Activity extends BaseActivity implements OnClickListener {

	private ViewPager viewPager;

	private PagerAdapter pagerAdapter;
	private List<View> views = new ArrayList<View>();

	// tab
	private LinearLayout tabWeixin;
	private LinearLayout tabfriend;
	private LinearLayout tabAddress;
	private LinearLayout tabSettings;

	private ImageButton weixinImg;
	private ImageButton friendImg;
	private ImageButton addressImg;
	private ImageButton settingImg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tab1);
		// 设置顶部标题栏
		setBackBtnTitleBar(R.string.title_activity_tab1);

		initView();

		initEvents();
	}

	/**
	 * 
	 */
	@SuppressLint("InflateParams")
	private void initView() {
		viewPager = (ViewPager) findViewById(R.id.tab_viewPager_id);
		// tabs
		tabWeixin = (LinearLayout) findViewById(R.id.tab_bottom_weixin_id);
		tabfriend = (LinearLayout) findViewById(R.id.tab_bottom_friend_id);
		tabAddress = (LinearLayout) findViewById(R.id.tab_bottom_address_id);
		tabSettings = (LinearLayout) findViewById(R.id.tab_bottom_setting_id);
		// imagebutton
		weixinImg = (ImageButton) findViewById(R.id.tab_bottom_weixin_image_id);
		friendImg = (ImageButton) findViewById(R.id.tab_bottom_friend_image_id);
		addressImg = (ImageButton) findViewById(R.id.tab_bottom_address_image_id);
		settingImg = (ImageButton) findViewById(R.id.tab_bottom_setting_image_id);

		LayoutInflater inflater = LayoutInflater.from(this);
		View tab01 = inflater.inflate(R.layout.tab1_tab1, null);
		View tab02 = inflater.inflate(R.layout.tab1_tab2, null);
		View tab03 = inflater.inflate(R.layout.tab1_tab3, null);
		View tab04 = inflater.inflate(R.layout.tab1_tab4, null);

		views.add(tab01);
		views.add(tab02);
		views.add(tab03);
		views.add(tab04);

		pagerAdapter = new PagerAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * android.support.v4.view.PagerAdapter#destroyItem(android.view
			 * .ViewGroup, int, java.lang.Object)
			 */
			@Override
			public void destroyItem(ViewGroup container, int position,
					Object object) {
				container.removeView(views.get(position));
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * android.support.v4.view.PagerAdapter#instantiateItem(android.
			 * view.ViewGroup, int)
			 */
			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				View view = views.get(position);
				container.addView(view);
				return view;
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}

			@Override
			public int getCount() {
				return views.size();
			}
		};

		viewPager.setAdapter(pagerAdapter);

	}

	/**
	 * 
	 */
	private void initEvents() {
		tabWeixin.setOnClickListener(this);
		tabfriend.setOnClickListener(this);
		tabAddress.setOnClickListener(this);
		tabSettings.setOnClickListener(this);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				int currentItem = viewPager.getCurrentItem();
				resetImg();
				switch (currentItem) {
				case 0:
					weixinImg.setImageResource(R.mipmap.tab_weixin_pressed);
					break;
				case 1:
					friendImg.setImageResource(R.mipmap.tab_find_frd_pressed);
					break;
				case 2:
					addressImg.setImageResource(R.mipmap.tab_address_pressed);
					break;
				case 3:
					settingImg.setImageResource(R.mipmap.tab_settings_pressed);
					break;

				default:
					break;
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		resetImg();
		switch (v.getId()) {
		case R.id.tab_bottom_weixin_id:
			viewPager.setCurrentItem(0);
			weixinImg.setImageResource(R.mipmap.tab_weixin_pressed);
			break;
		case R.id.tab_bottom_friend_id:
			viewPager.setCurrentItem(1);
			friendImg.setImageResource(R.mipmap.tab_find_frd_pressed);
			break;
		case R.id.tab_bottom_address_id:
			viewPager.setCurrentItem(2);
			addressImg.setImageResource(R.mipmap.tab_address_pressed);
			break;
		case R.id.tab_bottom_setting_id:
			viewPager.setCurrentItem(3);
			settingImg.setImageResource(R.mipmap.tab_settings_pressed);
			break;

		default:
			break;
		}

	}

	/**
	 * 将所有的图片切换为暗色
	 */
	private void resetImg() {
		weixinImg.setImageResource(R.mipmap.tab_weixin_normal);
		friendImg.setImageResource(R.mipmap.tab_find_frd_normal);
		addressImg.setImageResource(R.mipmap.tab_address_normal);
		settingImg.setImageResource(R.mipmap.tab_settings_normal);
	}

}
