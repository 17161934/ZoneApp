package net.zoneland.zoneapp;

import net.zoneland.zoneapp.view.SlidingView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 仿qq5.0 侧滑菜单演示界面
 * 
 * @author FancyLou
 * @date 2015年4月21日
 *
 */
public class MainActivity extends Activity {

	// 左边菜单
	private SlidingView leftMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		// 菜单ListView 数据适配器
		ListView menu = (ListView) findViewById(R.id.menu_list);
		menu.setAdapter(new MenuItemAdapter(this));

		leftMenu = (SlidingView) findViewById(R.id.leftMenuView);

		menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int i, long l) {
				Log.i("MainActivity", "menu_item:" + i);
				if (i == 0) {
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, TreeActivity.class);
					startActivity(intent);
				}
				if (i == 1) {
					Intent intent = new Intent();
					intent.setClass(MainActivity.this,
							CommonAdapterActivity.class);
					startActivity(intent);
				}
				if (i == 2) {
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, Tab1Activity.class);
					startActivity(intent);
				}
			}
		});
	}

	/**
	 * 切换菜单
	 * 
	 * @param v
	 */
	public void toggleMenu(View v) {
		leftMenu.toggleMenu();
	}

	private class MenuItemAdapter extends BaseAdapter {

		private Context mContext;

		private MenuItemAdapter(Context mContext) {
			this.mContext = mContext;
		}

		@Override
		public int getCount() {
			return texts.length;
		}

		@Override
		public Object getItem(int i) {
			return null;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int i, View view, ViewGroup viewGroup) {
			// 优化ListView
			if (view == null) {
				view = LayoutInflater.from(mContext).inflate(
						R.layout.menu_item, null);
				ItemViewCache viewCache = new ItemViewCache();
				viewCache.mTextView = (TextView) view
						.findViewById(R.id.id_menu_text);
				viewCache.mImageView = (ImageView) view
						.findViewById(R.id.id_menu_img);
				viewCache.mImageViewRight = (ImageView) view
						.findViewById(R.id.id_menu_arrow_right);
				view.setTag(viewCache);
			}
			ItemViewCache cache = (ItemViewCache) view.getTag();
			// 设置文本和图片，然后返回这个View，用于ListView的Item的展示
			cache.mTextView.setText(texts[i]);
			cache.mImageView.setImageResource(images[i]);
			cache.mImageViewRight.setImageResource(R.mipmap.arrow_right);
			return view;
		}

	}

	// 元素的缓冲类,用于优化ListView
	private static class ItemViewCache {
		public TextView mTextView;
		public ImageView mImageView;
		public ImageView mImageViewRight;
	}

	// 菜单名
	private int[] texts = new int[] { R.string.menu_text_1,
			R.string.menu_text_2, R.string.menu_text_3, R.string.menu_text_4,
			R.string.menu_text_5 };
	// 菜单的图片
	private int[] images = new int[] { R.mipmap.menu_tree,
			R.mipmap.menu_adapter, R.mipmap.img_3, R.mipmap.img_4,
			R.mipmap.img_5 };

}
