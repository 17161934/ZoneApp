package net.zoneland.zoneapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

/**
 * 应用启动界面
 * 
 * @author FancyLou
 * @date 2015年5月25日 下午8:38:08
 *
 */
public class SplashActivity extends Activity {

	private final int SPLASH_DISPLAY_LENGHT = 3000; // 延迟三秒

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final View view = View.inflate(this, R.layout.activity_splash, null);
		setContentView(view);

		AlphaAnimation animation = new AlphaAnimation(0.3f, 1.0f);
		animation.setDuration(SPLASH_DISPLAY_LENGHT);
		view.startAnimation(animation);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				redirectTo();
			}
		});

		// new Handler().postDelayed(new Runnable() {
		// public void run() {
		// Intent mainIntent = new Intent(SplashActivity.this,
		// MainActivity.class);
		// SplashActivity.this.startActivity(mainIntent);
		// SplashActivity.this.finish();
		// }
		//
		// }, SPLASH_DISPLAY_LENGHT);
	}

	public void redirectTo() {
		Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
		SplashActivity.this.startActivity(mainIntent);
		SplashActivity.this.finish();
	}
}
