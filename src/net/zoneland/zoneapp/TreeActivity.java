package net.zoneland.zoneapp;

import java.util.ArrayList;
import java.util.List;

import net.zoneland.zoneapp.common.tree.Node;
import net.zoneland.zoneapp.common.tree.TreeListViewAdapter;
import net.zoneland.zoneapp.tree.SimpleTreeListViewAdapter;
import net.zoneland.zoneapp.tree.bean.OrgBean;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 树形控件演示界面
 * 
 * @author FancyLou
 * @date 2015年4月21日 下午10:55:28
 *
 */
public class TreeActivity extends BaseActivity {

	private ListView tree;
	private SimpleTreeListViewAdapter<OrgBean> adapter;

	private List<OrgBean> orgs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tree);
		// 设置顶部标题栏
		setBackBtnTitleBar(R.string.title_activity_tree);

		tree = (ListView) findViewById(R.id.tree_list_view);

		// 初始化数据
		initDatas();

		try {
			adapter = new SimpleTreeListViewAdapter<OrgBean>(tree, this, orgs,
					0);
			tree.setAdapter(adapter);
		} catch (Exception e) {
			e.printStackTrace();
		}

		adapter.setOnTreeNodeClickListener(new TreeListViewAdapter.OnTreeNodeClickListener() {
			@Override
			public void onClick(Node node, int position) {
				if (node.isLeaf()) {
					Toast.makeText(TreeActivity.this, node.getName(),
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		tree.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView,
					View view, final int position, long l) {
				// 推荐 DialogFragment
				final EditText editText = new EditText(TreeActivity.this);
				new AlertDialog.Builder(TreeActivity.this)
						.setTitle(R.string.add_tree_item_title)
						.setView(editText)
						.setPositiveButton(R.string.add_tree_button_sure,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(
											DialogInterface dialogInterface,
											int which) {
										if (TextUtils.isEmpty(editText
												.getText())) {
											return;
										}
										adapter.addExtraNode(position, editText
												.getText().toString());
									}
								})
						.setNegativeButton(R.string.add_tree_button_cancel,
								null).show();
				return false;
			}
		});
	}

	private void initDatas() {
		orgs = new ArrayList<OrgBean>();
		OrgBean org1 = new OrgBean(1, 0, "根目录1");
		OrgBean org2 = new OrgBean(2, 0, "根目录2");
		OrgBean org3 = new OrgBean(3, 0, "根目录3");
		OrgBean org4 = new OrgBean(4, 0, "根目录4");

		OrgBean org1_1 = new OrgBean(5, 1, "根目录1-1");
		OrgBean org1_2 = new OrgBean(6, 1, "根目录1-2");
		OrgBean org1_1_1 = new OrgBean(7, 5, "根目录1-1-1");

		OrgBean org2_1 = new OrgBean(8, 2, "根目录2-1");

		orgs.add(org1);
		orgs.add(org2);
		orgs.add(org3);
		orgs.add(org4);
		orgs.add(org1_1);
		orgs.add(org1_2);
		orgs.add(org1_1_1);
		orgs.add(org2_1);

	}

}
