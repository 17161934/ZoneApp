/**
 * zoneland.net
 */
package net.zoneland.zoneapp.tree.bean;

import net.zoneland.zoneapp.common.tree.annotation.TreeNodeId;
import net.zoneland.zoneapp.common.tree.annotation.TreeNodeName;
import net.zoneland.zoneapp.common.tree.annotation.TreeNodePid;

/**
 *
 * @author FancyLou
 * @date 2015年4月21日 下午10:49:16
 *
 */
public class OrgBean {

	@TreeNodeId
	private int id;//
	@TreeNodePid
	private int pId;// 父id
	@TreeNodeName
	private String name;

	private String description;

	public OrgBean(int id, int pId, String name) {
		this.id = id;
		this.pId = pId;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
