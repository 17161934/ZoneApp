package net.zoneland.zoneapp;

import java.util.ArrayList;
import java.util.List;

import net.zoneland.zoneapp.common.adapter.CommonAdapter;
import net.zoneland.zoneapp.common.adapter.ViewHolder;
import net.zoneland.zoneapp.common.adapter.bean.TestBean;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;

/**
 * 通用适配器 演示界面
 * 
 * @author FancyLou
 * @date 2015年4月26日 下午8:38:35
 *
 */
public class CommonAdapterActivity extends BaseActivity {

	private ListView listView;

	private List<TestBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_common_adapter);
		// 设置顶部标题栏
		setBackBtnTitleBar(R.string.title_activity_common_adapter);

		initDatas();

		initView();

	}

	/**
	 * 初始化数据
	 */
	private void initDatas() {
		list = new ArrayList<TestBean>();
		TestBean t1 = new TestBean("Android新技能GET 1",
				"Android打造万能的ListView和GridView适配器", "2015-04-22", "13444445555");
		list.add(t1);
		TestBean t2 = new TestBean("Android新技能GET 2",
				"Android打造万能的ListView和GridView适配器", "2015-04-23", "13444445552");
		list.add(t2);
		TestBean t3 = new TestBean("Android新技能GET 3",
				"Android打造万能的ListView和GridView适配器", "2015-04-23", "13444445553");
		list.add(t3);
		TestBean t4 = new TestBean("Android新技能GET 4",
				"Android打造万能的ListView和GridView适配器", "2015-04-24", "13444445553");
		list.add(t4);
		TestBean t5 = new TestBean("Android新技能GET 5",
				"Android打造万能的ListView和GridView适配器", "2015-04-25", "13444445555");
		list.add(t5);
		TestBean t6 = new TestBean("Android新技能GET 6",
				"Android打造万能的ListView和GridView适配器", "2015-04-26", "13444445555");
		list.add(t6);
		TestBean t7 = new TestBean("Android新技能GET 7",
				"Android打造万能的ListView和GridView适配器", "2015-04-27", "13444445557");
		list.add(t7);

	}

	/**
	 * 初始化listview
	 */
	private void initView() {
		listView = (ListView) findViewById(R.id.common_list_id);
		listView.setAdapter(new CommonAdapter<TestBean>(
				CommonAdapterActivity.this, list,
				R.layout.common_adapter_listview_item) {

			@Override
			public void convert(ViewHolder holder, TestBean t) {
				holder.setText(R.id.common_adapter_listview_item_text_title_id,
						t.getTitle())
						.setText(
								R.id.common_adapter_listview_item_text_desc_id,
								t.getDesc())
						.setText(
								R.id.common_adapter_listview_item_text_time_id,
								t.getTime())
						.setText(
								R.id.common_adapter_listview_item_text_phone_id,
								t.getPhone());
			}
		});
	}

}
