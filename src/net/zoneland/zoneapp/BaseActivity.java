/**
 * zoneland.net Inc
 * Copyright (c) 2015 
 */
package net.zoneland.zoneapp;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * 基础activity 写一些通用的方法
 * 
 * @author FancyLou
 * @date 2015年4月26日 下午8:47:11
 *
 */
public class BaseActivity extends Activity {

	protected ImageButton backBtn;
	protected TextView topTitle;

	/**
	 * 设置通用的顶部标题栏
	 * 
	 * @param titleString
	 *            顶部标题栏的标题
	 */
	protected void setBackBtnTitleBar(int titleString) {
		// 顶部返回按钮
		backBtn = (ImageButton) findViewById(R.id.top_title_image_btn_left);
		backBtn.setVisibility(View.VISIBLE);
		backBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// 顶部菜单标题
		topTitle = (TextView) findViewById(R.id.top_title_text_id);
		topTitle.setText(titleString);
	}

}
