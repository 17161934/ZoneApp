package net.zoneland.zoneapp.common.tree;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.zoneland.zoneapp.R;
import net.zoneland.zoneapp.common.tree.annotation.TreeNodeId;
import net.zoneland.zoneapp.common.tree.annotation.TreeNodeName;
import net.zoneland.zoneapp.common.tree.annotation.TreeNodePid;

/**
 * 树形数据转化工具类 Created by FancyLou on 2015/4/15.
 */
public class TreeHelper {

	/**
	 * 将数据转换为树形数据
	 *
	 * 通过实体bean的注解解析数据对象生成Node
	 *
	 * @param datas
	 * @param <T>
	 * @return
	 */
	public static <T> List<Node> coverDatas2Nodes(List<T> datas)
			throws IllegalAccessException {

		List<Node> nodes = new ArrayList<Node>();
		Node node = null;
		for (T t : datas) {
			int id = -1;
			int pId = -1;
			String name = null;
			@SuppressWarnings("rawtypes")
			Class clazz = t.getClass();
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				if (field.getAnnotation(TreeNodeId.class) != null) {
					field.setAccessible(true);
					id = field.getInt(t);
				}
				if (field.getAnnotation(TreeNodePid.class) != null) {
					field.setAccessible(true);
					pId = field.getInt(t);
				}
				if (field.getAnnotation(TreeNodeName.class) != null) {
					field.setAccessible(true);
					name = (String) field.get(t);
				}
			}
			node = new Node(id, pId, name);
			nodes.add(node);
		}

		// 设置node的关联关系 上下级关系 方便listView排序
		for (int i = 0; i < nodes.size(); i++) {
			Node n = nodes.get(i);

			for (int j = i + 1; j < nodes.size(); j++) {
				Node m = nodes.get(j);

				if (m.getpId() == n.getId()) {
					n.getChildren().add(m);
					m.setParent(n);
				} else if (m.getId() == n.getpId()) {
					m.getChildren().add(n);
					n.setParent(m);
				}
			}
		}

		for (Node n : nodes) {
			setNodeIcon(n);
		}

		return nodes;
	}

	/**
	 * 排序
	 * 
	 * @param datas
	 * @param <T>
	 * @return
	 * @throws IllegalAccessException
	 */
	public static <T> List<Node> sortNodes(List<T> datas, int defaultExpandLevel)
			throws IllegalAccessException {
		List<Node> result = new ArrayList<Node>();
		List<Node> nodes = coverDatas2Nodes(datas);

		// 先拿到所有的根节点
		List<Node> rootNodes = getRootNodes(nodes);

		for (Node node : rootNodes) {
			addNode(result, node, defaultExpandLevel, 1);
		}

		return result;
	}

	/**
	 * 过滤出所有显示的节点
	 * 
	 * @param nodes
	 * @return
	 */
	public static List<Node> filterVisibleNodes(List<Node> nodes) {
		List<Node> result = new ArrayList<Node>();
		for (Node node : nodes) {
			if (node.isRoot() || node.isParentExpand()) {
				setNodeIcon(node);
				result.add(node);
			}
		}
		return result;
	}

	/**
	 * 把一个节点的所有孩子节点都放入result
	 * 
	 * @param result
	 * @param node
	 * @param defaultExpandLevel
	 *            默认展开到第几层
	 * @param currentLevel
	 *            当前节点level
	 */
	private static void addNode(List<Node> result, Node node,
			int defaultExpandLevel, int currentLevel) {
		result.add(node);
		if (defaultExpandLevel >= currentLevel) {
			node.setExpand(true);
		}
		if (node.isLeaf()) {
			return;
		}
		for (int i = 0; i < node.getChildren().size(); i++) {
			addNode(result, node.getChildren().get(i), defaultExpandLevel,
					currentLevel + 1);
		}

	}

	/**
	 * 从所有节点中过滤出根节点
	 * 
	 * @param nodes
	 * @return
	 */
	private static List<Node> getRootNodes(List<Node> nodes) {
		List<Node> root = new ArrayList<Node>();
		for (Node node : nodes) {
			if (node.isRoot()) {
				root.add(node);
			}
		}
		return root;
	}

	/**
	 * 动态设置图标
	 * 
	 * @param node
	 */
	private static void setNodeIcon(Node node) {
		if (node.getChildren().size() > 0 && node.isExpand()) {
			node.setIcon(R.mipmap.tree_expand);// 展开箭头
		} else if (node.getChildren().size() > 0 && !node.isExpand()) {
			node.setIcon(R.mipmap.tree_close);// 收缩箭头
		} else {
			node.setIcon(R.mipmap.tree_leaf);// 没有图标
		}
	}
}
