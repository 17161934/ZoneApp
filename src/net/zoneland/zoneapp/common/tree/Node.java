/**
 * zoneland.net
 */
package net.zoneland.zoneapp.common.tree;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FancyLou
 * @date 2015年4月21日 下午10:50:25
 *
 */
public class Node {

	public Node() {
	}

	public Node(int id, int pId, String name) {
		this.id = id;
		this.pId = pId;
		this.name = name;
	}

	private int id;
	/**
	 * 根节点为0
	 */
	private int pId = 0;

	private String name;
	/**
	 * 树的层级
	 */
	@SuppressWarnings("unused")
	private int level;

	/**
	 * 是否展开
	 */
	private boolean isExpand = false;

	private int icon;// 图标

	private Node parent;

	private List<Node> children = new ArrayList<Node>();

	/**
	 * 判断是否是根节点
	 * 
	 * @return
	 */
	public boolean isRoot() {
		return parent == null;
	}

	/**
	 * 判断当前父节点的收缩状态
	 * 
	 * @return
	 */
	public boolean isParentExpand() {
		if (parent == null) {
			return false;
		}
		return parent.isExpand();
	}

	/**
	 * 是否是叶子节点 叶子节点不显示图标
	 * 
	 * @return
	 */
	public boolean isLeaf() {
		return children.size() == 0;
	}

	/**
	 * 计算层级
	 * 
	 * @return
	 */
	public int getLevel() {
		return parent == null ? 0 : parent.getLevel() + 1;
	}

	public boolean isExpand() {
		return isExpand;
	}

	public void setExpand(boolean isExpand) {
		this.isExpand = isExpand;
		if (!isExpand) {
			for (Node child : children) {
				child.setExpand(false);
			}
		}
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

}
