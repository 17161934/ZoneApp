/**
 * zoneland.net Inc
 * Copyright (c) 2015 
 */
package net.zoneland.zoneapp.common.adapter;

import java.util.List;

import net.zoneland.zoneapp.R;
import net.zoneland.zoneapp.common.adapter.bean.TestBean;
import android.content.Context;

/**
 * 测试用的adapter
 * 
 * @author FancyLou
 * @date 2015年4月26日 下午9:53:18
 *
 */
public class MyTestAdapterWithCommonViewHolder extends CommonAdapter<TestBean> {

	/**
	 * 
	 */
	public MyTestAdapterWithCommonViewHolder(Context context,
			List<TestBean> datas) {
		super(context, datas, R.layout.common_adapter_listview_item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zoneland.zoneapp.common.adapter.CommonAdapter#convert(net.zoneland
	 * .zoneapp.common.adapter.ViewHolder, java.lang.Object)
	 */
	@Override
	public void convert(ViewHolder holder, TestBean t) {
		holder.setText(R.id.common_adapter_listview_item_text_title_id,
				t.getTitle())
				.setText(R.id.common_adapter_listview_item_text_desc_id,
						t.getDesc())
				.setText(R.id.common_adapter_listview_item_text_time_id,
						t.getTime())
				.setText(R.id.common_adapter_listview_item_text_phone_id,
						t.getPhone());

	}

}
