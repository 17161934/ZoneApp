/**
 * zoneland.net Inc
 * Copyright (c) 2015 
 */
package net.zoneland.zoneapp.common.adapter.bean;

/**
 * 测试用的bean
 * 
 * @author FancyLou
 * @date 2015年4月26日 下午9:07:59
 *
 */
public class TestBean {

	private String title;
	private String desc;
	private String time;
	private String phone;

	/**
	 * 
	 */
	public TestBean() {
	}

	/**
	 * @param title
	 * @param desc
	 * @param time
	 * @param phone
	 */
	public TestBean(String title, String desc, String time, String phone) {
		this.title = title;
		this.desc = desc;
		this.time = time;
		this.phone = phone;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

}
